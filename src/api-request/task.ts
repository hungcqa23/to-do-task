import { CreateTaskBody } from "@/components/task-input";
import http from "@/lib/http";
import { ListTasksResType, TaskType } from "@/schemas/task";

const taskApiRequest = {
  getAllTasks: async () =>
    http.get<ListTasksResType>("/tasks?populate=*", {
      headers: {
        Authorization: `Bearer 78c101dc86feb23c8b116b8572ee5266b07c8d1748cc2f7d37d0a2a7970a0ea3f38fff3d2089f5267e239246a40e849bba7d381125d88fbfbecabb3fe59bbaa408c923ea1c75aaf04d5fddd0bd4a9855c6fe2dc1f1c375127a0bfab23bece5b524744841d4aa3ee218157984009d9a7eda83ea0616a6e765ad41572f0dd5471b`,
      },
      cache: "no-cache",
    }),
  getTask: async (id: string) =>
    fetch(`http://localhost:8000/api/tasks/${id}?populate=*`, {
      headers: {
        Authorization: `Bearer 78c101dc86feb23c8b116b8572ee5266b07c8d1748cc2f7d37d0a2a7970a0ea3f38fff3d2089f5267e239246a40e849bba7d381125d88fbfbecabb3fe59bbaa408c923ea1c75aaf04d5fddd0bd4a9855c6fe2dc1f1c375127a0bfab23bece5b524744841d4aa3ee218157984009d9a7eda83ea0616a6e765ad41572f0dd5471b`,
      },
    }),
  createTask: async (
    body: CreateTaskBody & {
      status: number[];
      type: number[];
    }
  ) =>
    http.post<{
      data: TaskType;
    }>(
      "/tasks",
      {
        data: body,
      },
      {
        headers: {
          Authorization: `Bearer 78c101dc86feb23c8b116b8572ee5266b07c8d1748cc2f7d37d0a2a7970a0ea3f38fff3d2089f5267e239246a40e849bba7d381125d88fbfbecabb3fe59bbaa408c923ea1c75aaf04d5fddd0bd4a9855c6fe2dc1f1c375127a0bfab23bece5b524744841d4aa3ee218157984009d9a7eda83ea0616a6e765ad41572f0dd5471b`,
        },
        cache: "no-cache",
      }
    ),
  uploadImage: async (data: FormData) =>
    http.post<{
      data: TaskType;
    }>("/upload", data, {
      headers: {
        Authorization: `Bearer 78c101dc86feb23c8b116b8572ee5266b07c8d1748cc2f7d37d0a2a7970a0ea3f38fff3d2089f5267e239246a40e849bba7d381125d88fbfbecabb3fe59bbaa408c923ea1c75aaf04d5fddd0bd4a9855c6fe2dc1f1c375127a0bfab23bece5b524744841d4aa3ee218157984009d9a7eda83ea0616a6e765ad41572f0dd5471b`,
      },
      cache: "no-cache",
    }),
};

export default taskApiRequest;
