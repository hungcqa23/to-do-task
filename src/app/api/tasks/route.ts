import http from "@/lib/http";

export async function POST(request: Request) {
  const body = await request.json();
  console.log(body);
  const response = await http.get("/tasks");
  console.log(response);

  return new Response("OK");
}
