import taskApiRequest from "@/api-request/task";
import List from "@/components/list";
import { Task } from "@/components/task";
import { TaskType } from "@/schemas/task";
import React from "react";

export default async function ListTasks() {
  const res = await taskApiRequest.getAllTasks();
  return (
    <div>
      {List<TaskType>({
        listItems: res.payload.data,
        mapFn: (task: TaskType, index: number) => (
          <Task key={task.id} task={task} />
        ),
        className: "flex flex-col gap-2",
      })}
    </div>
  );
}
