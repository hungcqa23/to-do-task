import ListTasks from "@/app/list-tasks";
import TaskInput from "@/components/task-input";

export default async function Home() {
  return (
    <div className="w-full h-screen flex items-center justify-center">
      <div className="max-w-2xl flex flex-col items-center gap-4">
        <TaskInput />
        <ListTasks />
      </div>
    </div>
  );
}
