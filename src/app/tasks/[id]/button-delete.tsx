"use client";

import { Button } from "@/components/ui/button";
import React from "react";

export default function ButtonDelete({ id }: { id: string }) {
  const onDeleteMutation = async () => {
    const response = await fetch(`http://localhost:8000/api/tasks/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer 78c101dc86feb23c8b116b8572ee5266b07c8d1748cc2f7d37d0a2a7970a0ea3f38fff3d2089f5267e239246a40e849bba7d381125d88fbfbecabb3fe59bbaa408c923ea1c75aaf04d5fddd0bd4a9855c6fe2dc1f1c375127a0bfab23bece5b524744841d4aa3ee218157984009d9a7eda83ea0616a6e765ad41572f0dd5471b`,
      },
    });

    if (response.ok) {
      window.location.href = "/";
    }
  };

  return (
    <Button
      variant={"outline"}
      className="text-red-500 border hover:text-red-500"
      onClick={onDeleteMutation}>
      Delete
    </Button>
  );
}
