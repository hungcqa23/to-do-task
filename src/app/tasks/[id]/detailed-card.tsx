import ButtonDelete from "@/app/tasks/[id]/button-delete";
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Label } from "@/components/ui/label";
import { formatDateTime } from "@/lib/helper";
import { TaskType } from "@/schemas/task";
import Image from "next/image";

export default function DetailedCard({ task }: { task: TaskType }) {
  return (
    <div className="h-screen w-screen flex justify-center items-center">
      <Card className="w-[350px]">
        <CardHeader>
          <CardTitle>{task.attributes?.title}</CardTitle>
          {task.attributes.description && (
            <CardDescription>{task.attributes.description}</CardDescription>
          )}
        </CardHeader>

        <CardContent>
          <div className="flex flex-col gap-4">
            <div className="flex items-baseline gap-2">
              <Label>Status:</Label>
              <p>Done</p>
            </div>
            <div className="flex items-baseline gap-2">
              <Label>Type:</Label>
              <p>Job</p>
            </div>

            <div className="flex items-baseline gap-2">
              <Label>Created Date:</Label>
              <p>{formatDateTime(task.attributes.updatedAt)}</p>
            </div>

            <div className="flex items-baseline gap-2">
              <Label>Due date:</Label>
              <p>{formatDateTime(task.attributes.finishTime)}</p>
            </div>

            {task.attributes.image.data !== null && (
              <div className="flex justify-center">
                <Image
                  src={task.attributes.image.data.attributes.url}
                  alt="image"
                  width={200}
                  height={200}
                />
              </div>
            )}
          </div>
        </CardContent>

        <CardFooter className="flex justify-between">
          <ButtonDelete id={task.id.toString()} />
          <Button>Update Card</Button>
        </CardFooter>
      </Card>
    </div>
  );
}
