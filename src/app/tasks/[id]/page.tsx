import DetailedCard from "@/app/tasks/[id]/detailed-card";
import taskApiRequest from "@/api-request/task";
import { TaskType } from "@/schemas/task";

export default async function Card({ params }: { params: { id: string } }) {
  const res = await taskApiRequest.getTask(params.id);
  const task = (await res.json()).data as TaskType;

  return <DetailedCard task={task} />;
}
