"use client";

import { TimePicker } from "@/components/time-picker";
import { Button } from "@/components/ui/button";
import { Calendar } from "@/components/ui/calendar";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Textarea } from "@/components/ui/textarea";
import { cn } from "@/lib/utils";
import { useGetAllStatuses } from "@/queries/useStatus";
import {
  useCreateTaskMutation,
  useUploadImageTaskMutation,
} from "@/queries/useTask";
import { useGetAllTypes } from "@/queries/useType";
import { StatusType } from "@/schemas/status";
import { TaskType } from "@/schemas/task";
import { TypeType } from "@/schemas/type";
import { zodResolver } from "@hookform/resolvers/zod";
import { DialogTrigger } from "@radix-ui/react-dialog";
import { format } from "date-fns";
import { CalendarIcon } from "lucide-react";
import React, { Dispatch, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";

const formSchema = z.object({
  title: z.string().min(1, { message: "Required" }).max(50),
  description: z.string().optional(),
  finishTime: z.date(),
});

export type CreateTaskBody = z.infer<typeof formSchema>;

export default function TaskInput() {
  const { data: statusesData, isLoading: isLoadingStatuses } =
    useGetAllStatuses();
  const { data: typesData, isLoading: isLoadingTypes } = useGetAllTypes();

  const createTaskMutation = useCreateTaskMutation();
  const uploadImageTaskMutation = useUploadImageTaskMutation();
  const [status, setStatus] = useState<string>("");
  const [type, setType] = useState<string>("");
  const [file, setFile] = useState<File | null>(null);
  const form = useForm<CreateTaskBody>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      title: "",
      description: "",
    },
  });

  // useEffect(() => {}, [uploadImageTaskMutation]);
  const onSubmit = async (values: CreateTaskBody) => {
    const res = await createTaskMutation.mutateAsync({
      title: values.title,
      description: values.description || "",
      status: [Number(status)],
      type: [Number(type)],
      finishTime: values.finishTime || undefined,
    });

    console.log("status", createTaskMutation.status);
    if (file && createTaskMutation.isSuccess) {
      console.log("Hello World!");
      const formData = new FormData();
      console.log("ID: ", res.payload.data.id);
      formData.append("ref", "api::task.task");
      formData.append("refId", `${res.payload.data.id}`);
      formData.append("field", "image");
      formData.append("files", file);
      console.log(formData.get("files"));
      console.log(formData.get("refId"));
      console.log(formData.get("field"));
      console.log(formData.get("ref"));
      const resUploadImage = await uploadImageTaskMutation.mutateAsync(
        formData
      );

      if (uploadImageTaskMutation.isSuccess) setFile(null);
      form.reset();
      setStatus("");
      setType("");
      form.setValue("finishTime", new Date());
    }
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          type="submit"
          variant="default"
          className="py-1 h-8 max-w-48 w-full">
          Add New Task
        </Button>
      </DialogTrigger>

      <DialogContent className="sm:max-w-[425px]">
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <DialogHeader>
              <DialogTitle className="text-center">Add New Task</DialogTitle>
            </DialogHeader>

            <div className="grid gap-4 py-4">
              <FormField
                control={form.control}
                name="title"
                render={({ field }) => (
                  <FormItem className="flex flex-col space-y-1.5">
                    <FormLabel>Title:</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Enter your title"
                        {...field}
                        className="flex-grow"
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name="description"
                render={({ field }) => (
                  <FormItem className="flex flex-col space-y-1.5">
                    <FormLabel htmlFor="username">Description:</FormLabel>
                    <FormControl>
                      <Textarea
                        placeholder="Type your message here."
                        {...field}
                      />
                    </FormControl>
                  </FormItem>
                )}
              />

              <div className="flex items-center gap-2">
                <Label htmlFor="picture" className="min-w-20">
                  Picture:
                </Label>
                <Input
                  id="picture"
                  type="file"
                  className="flex-grow"
                  onChange={e => {
                    if (e.target.files) {
                      setFile(e.target.files[0]);
                    }
                  }}
                />
              </div>

              <div className="flex items-center gap-2">
                <FormLabel className="min-w-20">Status:</FormLabel>
                <Select value={status} onValueChange={e => setStatus(e)}>
                  <SelectTrigger className="flex-grow">
                    <SelectValue placeholder="Select status" />
                  </SelectTrigger>

                  <SelectContent>
                    <SelectGroup>
                      {isLoadingStatuses && (
                        <SelectItem value="loading">Loading...</SelectItem>
                      )}

                      {!isLoadingStatuses &&
                        statusesData?.payload.data.map((status: StatusType) => (
                          <SelectItem key={status.id} value={`${status.id}`}>
                            {status.attributes.title}
                          </SelectItem>
                        ))}
                    </SelectGroup>
                  </SelectContent>
                </Select>
              </div>

              <div className="flex items-center gap-2">
                <Label className="min-w-20">Type:</Label>
                <Select value={type} onValueChange={e => setType(e)}>
                  <SelectTrigger className="flex-grow">
                    <SelectValue placeholder="Select type" />
                  </SelectTrigger>

                  <SelectContent>
                    <SelectGroup>
                      {isLoadingTypes && (
                        <SelectItem value="loading">Loading...</SelectItem>
                      )}

                      {!isLoadingTypes &&
                        typesData?.payload.data.map((type: TypeType) => (
                          <SelectItem key={type.id} value={`${type.id}`}>
                            {type.attributes.title}
                          </SelectItem>
                        ))}
                    </SelectGroup>
                  </SelectContent>
                </Select>
              </div>

              <FormField
                control={form.control}
                name="finishTime"
                render={({ field }) => (
                  <FormItem className="flex gap-2 items-center space-y-0">
                    <FormLabel className="text-left min-w-20">
                      Due date:
                    </FormLabel>

                    <Popover>
                      <FormControl>
                        <PopoverTrigger asChild>
                          <Button
                            variant="outline"
                            className={cn(
                              "w-[280px] justify-start text-left font-normal flex-grow",
                              !field.value && "text-muted-foreground"
                            )}>
                            <CalendarIcon className="mr-2 h-4 w-4" />
                            {field.value ? (
                              format(field.value, "PPP HH:mm:ss")
                            ) : (
                              <span>Pick a date</span>
                            )}
                          </Button>
                        </PopoverTrigger>
                      </FormControl>

                      <PopoverContent className="w-auto p-0">
                        <Calendar
                          mode="single"
                          selected={field.value}
                          onSelect={field.onChange}
                          initialFocus
                        />
                        <div className="p-3 border-t border-border">
                          <TimePicker
                            setDate={field.onChange}
                            date={field.value}
                          />
                        </div>
                      </PopoverContent>
                    </Popover>
                  </FormItem>
                )}
              />
            </div>

            <DialogFooter>
              <Button type="submit" className="w-full">
                Add new task
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
}
