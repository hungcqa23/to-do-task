import { RocketIcon } from "@radix-ui/react-icons";
import { Button } from "@/components/ui/button";
import { AlertTitle } from "@/components/ui/alert";
import { TaskType } from "@/schemas/task";
import { Check, CircleCheck, PencilLine, Trash, Undo2 } from "lucide-react";
import { calculateTimeAgo } from "@/lib/helper";
import Link from "next/link";

interface TaskProps {
  task: TaskType;
}
export function Task({ task }: TaskProps) {
  return (
    <Link href={`/tasks/${task.id}`}>
      <div className="border border-slate-300 rounded-md flex items-center px-4 py-2 gap-4 justify-between shadow hover:shadow-md transition-all duration-300">
        <div className="flex gap-2 flex-grow items-center">
          {!task.attributes.completed ? (
            <RocketIcon className="h-4 w-4" />
          ) : (
            <CircleCheck className="h-4 w-4" />
          )}
          <div className="flex items-baseline gap-2">
            <AlertTitle className="line-clamp-1 max-w-[250px] text-sm">
              {task.attributes.title}
            </AlertTitle>

            <p className="text-slate-500 text-xs">
              {calculateTimeAgo(task.attributes.updatedAt)}
            </p>
          </div>
        </div>

        {/* <div className="flex items-center gap-1">
          <Button size={"icon"} variant={"outline"} className="text-slate-500">
            {!task.attributes.completed ? (
              <Check size={16} />
            ) : (
              <Undo2 size={16} />
            )}
          </Button>

          <Button
            size={"icon"}
            variant={"outline"}
            className="text-slate-500"
            onClick={() => {}}>
            <PencilLine size={16} />
          </Button>
  
          <Button
            size={"icon"}
            variant={"outline"}
            className="text-slate-500"
            onClick={() => {
              // handleDeleteTask(task.id);
            }}>
            <Trash size={16} />
          </Button>
        </div> */}
      </div>
    </Link>
  );
}
