import statusApiRequest from "@/api-request/status";
import { useQuery } from "@tanstack/react-query";

export const useGetAllStatuses = () =>
  useQuery({
    queryKey: ["statuses"],
    queryFn: () => statusApiRequest.getAllStatuses(),
  });
