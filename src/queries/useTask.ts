import taskApiRequest from "@/api-request/task";
import { useMutation, useQueryClient } from "@tanstack/react-query";

export const useCreateTaskMutation = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: taskApiRequest.createTask,
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ["tasks"],
        exact: true,
      });
    },
  });
};

export const useUploadImageTaskMutation = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: taskApiRequest.uploadImage,
    onSuccess: () => {
      console.log("Hello World!");
      queryClient.invalidateQueries({
        queryKey: ["tasks"],
        exact: true,
      });
    },
  });
};
