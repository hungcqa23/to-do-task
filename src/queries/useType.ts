import typeApiRequest from "@/api-request/type";
import { useQuery } from "@tanstack/react-query";

export const useGetAllTypes = () =>
  useQuery({
    queryKey: ["types"],
    queryFn: () => typeApiRequest.getAllTypes(),
  });
