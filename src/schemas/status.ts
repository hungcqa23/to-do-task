import { z } from "zod";

const StatusSchema = z.object({
  id: z.number(),
  attributes: z.object({
    title: z.string(),
    createdAt: z.string(),
    updatedAt: z.string(),
  }),
});

export type StatusType = z.infer<typeof StatusSchema>;

// "meta": {
//   "pagination": {
//       "page": 1,
//       "pageSize": 25,
//       "pageCount": 1,
//       "total": 2
//   }
// }
export const ListStatusRes = z.object({
  data: z.array(StatusSchema),
  meta: z.object({
    pagination: z.object({
      page: z.number(),
      pageSize: z.number(),
      pageCount: z.number(),
      total: z.number(),
    }),
  }),
});

export type ListStatusResType = z.infer<typeof ListStatusRes>;
