import { z } from "zod";

const TaskSchema = z.object({
  id: z.number(),
  attributes: z.object({
    title: z.string(),
    completed: z.boolean().default(false),
    description: z.string().optional(),
    createdAt: z.string(),
    updatedAt: z.string(),
    finishTime: z.string(),
    status: z.object({
      data: z.object({
        id: z.string(),
        attribute: z.object({
          title: z.string(),
        }),
      }),
    }),
    type: z.object({
      data: z.object({
        id: z.string(),
        attribute: z.object({
          title: z.string(),
        }),
      }),
    }),
    image: z.object({
      data: z.object({
        id: z.string(),
        attributes: z.object({
          format: z.object({
            thumbnail: z.object({
              url: z.string(),
            }),
            small: z.object({
              url: z.string(),
            }),
          }),
          url: z.string(),
        }),
      }),
    }),
  }),
});

export type TaskType = z.infer<typeof TaskSchema>;

export const ListTasksRes = z.object({
  data: z.array(TaskSchema),
  meta: z.object({
    pagination: z.object({
      page: z.number(),
      pageSize: z.number(),
      pageCount: z.number(),
      total: z.number(),
    }),
  }),
});

export type ListTasksResType = z.infer<typeof ListTasksRes>;
