import { z } from "zod";

const TypeSchema = z.object({
  id: z.number(),
  attributes: z.object({
    title: z.string(),
    createdAt: z.string(),
    updatedAt: z.string(),
  }),
});

export type TypeType = z.infer<typeof TypeSchema>;

// "meta": {
//   "pagination": {
//       "page": 1,
//       "pageSize": 25,
//       "pageCount": 1,
//       "total": 2
//   }
// }
export const ListTypesRes = z.object({
  data: z.array(TypeSchema),
  meta: z.object({
    pagination: z.object({
      page: z.number(),
      pageSize: z.number(),
      pageCount: z.number(),
      total: z.number(),
    }),
  }),
});

export type ListTypesResType = z.infer<typeof ListTypesRes>;
